function [u, qr, ur] = Controller(q, t)

    [qr, ur] = TrajectoryGenerator(t);
    
    % Define control law
    
    u = ur;
end

