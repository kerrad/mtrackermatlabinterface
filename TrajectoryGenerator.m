function [qr, ur] = TrajectoryGenerator(t)

% Lissajou curves

% Parameters
A0x = 0; A0y = 0;
A1x = 0.25; A1y = 0.25;
wx = 1.0; wy = 1.0;
phix = 0; phiy = pi/2;

% Position variables
xr = A1x*sin(wx*t+phix) + A0x;
yr = A1y*sin(wy*t+phiy) + A0y;

% Compute these variables!
% dxr = 
% dyr = 
% ddxr = 
% ddyr = 

thr = 0;
vr = 0;
wr = 0;

qr = [xr; yr; thr];
ur = [vr; wr];

end

