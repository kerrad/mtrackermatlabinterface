function [qr, ur, dqr] = TrajectoryGenerator(t)

% Lissajou curves

% Parameters
A0x = 0; A0y = 0;
A1x = 0.25; A1y = 0.25;
wx = 0.6; wy = 0.3;
phix = 0; phiy = pi/2;

% Position variables
xr = A1x*sin(wx*t+phix) + A0x;
yr = A1y*sin(wy*t+phiy) + A0y;

% Compute these variables!
 dxr = A1x*cos(wx*t+phix)*wx;
 dyr = A1y*cos(wy*t+phiy)*wy;
 ddxr = -A1x*sin(wx*t+phix)*wx*wx;
 ddyr = -A1y*sin(wy*t+phiy)*wy*wy;

thr = atan2(dyr,dxr);
vr = sqrt(dxr*dxr+dyr*dyr);
wr = (ddyr*dxr - dyr*ddxr)/(dxr*dxr+dyr*dyr);

qr = [xr; yr; thr];
dqr = [dxr;dyr];

ur = [wr; vr];

end

