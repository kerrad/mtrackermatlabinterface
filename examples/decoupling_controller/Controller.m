function [u, qr, ur, e_i, z, zr] = Controller(q, t)

    [qr, ur, dqr] = TrajectoryGenerator(t);
    
    x = q(1);
    xr = qr(1);
    y = q(2);
    yr = qr(2);
    
    teta = q(3);
    
     
    dx=0.05;
    dy=0;
    k = 6;
    zr = [qr(1);qr(2)];
    %zr=[1;1];
    z = [cos(teta)*dx-sin(teta)*dy;sin(teta)*dx + cos(teta)*dy]+[q(1);q(2)];
    e_i = [z-zr; 0];
    
    
    P = [ -sin(teta)*dx-cos(teta)*dy cos(teta);  cos(teta)*dx-sin(teta)*dy sin(teta)];
    u = inv(P)*(-k*(z-zr)+dqr);
    %u = ur;
end

