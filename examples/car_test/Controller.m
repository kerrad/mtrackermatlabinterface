function [u] = Controller(q, t)

    vel = 1; % 5*sin(0.5*t);
    phi = 0.8*cos(3*t);
    u = [vel; phi];
end

