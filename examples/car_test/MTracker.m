%*************************************************************************
% Matlab interface for MTracker Car robot
% (c) IAR, D. Pazderski 2021
%*************************************************************************

if (MTrackerCarDriver('open', [3, 921600]) == -1)
    return;
end

Tf = 20; %czas koncowy
Ts = 0.03; %czas probkowania

n = floor(Tf/Ts)+1;

% Initialization of buffers to store data
t = zeros(1, n);
dataReady = zeros(1, n);
w = zeros(2, n);
wd = zeros(2, n);
ud = zeros(2, n);
ur = zeros(2, n);
q = zeros(3, n);
qr = zeros(3, n);
uc = zeros(1, n);

% Define user data
e = zeros(3,n);
z = zeros(2,n);
zr = zeros(2,n);

% Initial localization
q_i = [0; 0; 0];
w_i = [0; 0];
uc_i = [0];

i = 0; tau = 0;
tic;

MTrackerCarDriver('setOdometry', q_i); 
wait(tau, 0.1);
data = MTrackerCarDriver('read');

disp('Robot is started.');
% Main control loop
while (tau < Tf)
    
    i = i+1;
    tau = toc;

    % Compute control law
    [ud_i] = Controller(q_i, tau);
    wd_i = ud_i;
            
    % Communication with the robot
    MTrackerCarDriver('sendVelocity', wd_i); 
    wait(tau, Ts);
    data = MTrackerCarDriver('read');
    
    % Check if new data is available
    if (data(1) == 1)
        q_i = data(2:4)';
        w_i = data(5)';
        uc_i = data(6)';
    end
    
    dataReady(:,i) = data(1);
    q(:,i) = q_i;
    w(:,i) = w_i; 
    wd(:,i) = wd_i;
    ud(:,i) = ud_i;
    t(i) = tau;  
    uc(:,i) = uc_i;
end    

MTrackerCarDriver('close');

% Adjust buffers
t = t(:, 1:i);
w = w(:, 1:i);
wd = wd(:, 1:i);
ud = ud(:, 1:i);
ur = ur(:, 1:i);
qr = qr(:, 1:i);
q = q(:, 1:i);
uc = uc(:, 1:i);

disp('Robot is stopped.');