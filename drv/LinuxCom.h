// C library headers
#include <stdio.h>
#include <string.h>
#include "mex.h"

// Linux headers
#include <fcntl.h> // Contains file controls like O_RDWR
#include <errno.h> // Error integer and strerror() function
#include <termios.h> // Contains POSIX terminal control definitions
#include <unistd.h> // write(), read(), close()

class SystemCom  
{
private:
    int port_handle;
	char port_name[16];					
	int port_number;

	int state;
public:
	static const int buf_size = 1024;
    int error;
	bool is_closed;
	unsigned char buffer_in[buf_size];						// bufor wejťciowy
    
	SystemCom()
	{
        error = 0;
		is_closed = true;
		state = 1;
	}

	bool Send(const unsigned char *ptr, size_t len)
	{
		if(is_closed) {
			sleep(1);
			return true;	// jeśli port zaknięty to wychodzimy
		}

        tcflush(port_handle,TCIOFLUSH);

        if(write(port_handle, ptr, len) < 0) 
        {
            error = errno;
            //mexPrintf(strerror(error));
            return false;
        }
        //tcdrain(port_handle);

        //char s[16];
        //sprintf(s,"%ld", port_handle);
        //mexPrintf(s);

		return true;
	}

	int Receive()
	{
        //char s[16];
        //sprintf(s,"%ld", port_handle);
        //mexPrintf(s);

        int ret = read(port_handle, &buffer_in, buf_size);
        if (ret < 0) {
            error = errno;
            //mexPrintf(strerror(error));
        }
        return ret;
	}

	void Close()
	{
		if( !is_closed )
			close(port_handle);

		is_closed = true;
	}

	int Open(int p, int baudrate)
	{
		if (!is_closed) return 0;

		//	ustalenie nazwy
        port_number = p;
        sprintf(port_name, "/dev/ttyUSB%d", port_number);

        port_handle = open(port_name, O_RDWR);

        //char s[16];
        //sprintf(s,"%d", port_handle);
        //mexPrintf(s);

        if (port_handle < 0) {
            error = errno;
            //mexPrintf(strerror(error));
            return error;
        }

		// parametry transmmisji
        struct termios tty;
        if(tcgetattr(port_handle, &tty) != 0) {
            error = errno;
            //mexPrintf(strerror(error));
            return error;
        }

        tty.c_cflag &= ~PARENB; // Clear parity bit, disabling parity (most common)
        tty.c_cflag &= ~CSTOPB; // Clear stop field, only one stop bit used in communication (most common)
        tty.c_cflag |= CS8; // 8 bits per byte (most common)
        tty.c_cflag &= ~CRTSCTS; // Disable RTS/CTS hardware flow control (most common)
        tty.c_cflag |= CREAD | CLOCAL; // Turn on READ & ignore ctrl lines (CLOCAL = 1)

        tty.c_lflag &= ~ICANON;
        tty.c_lflag &= ~ECHO; // Disable echo
        tty.c_lflag &= ~ECHOE; // Disable erasure
        tty.c_lflag &= ~ECHONL; // Disable new-line echo
        tty.c_lflag &= ~ISIG; // Disable interpretation of INTR, QUIT and SUSP

        tty.c_iflag &= ~(IXON | IXOFF | IXANY); // Turn off s/w flow ctrl
        tty.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL); // Disable any special handling of received bytes
    
        tty.c_oflag &= ~OPOST; // Prevent special interpretation of output bytes (e.g. newline chars)
        tty.c_oflag &= ~ONLCR; // Prevent conversion of newline to carriage return/line feed

        tty.c_cc[VTIME] = 10;    // Wait for up to 1s (10 deciseconds), returning as soon as any data is received.
        tty.c_cc[VMIN] = 0;

        cfsetispeed(&tty, get_baud(baudrate));
        cfsetospeed(&tty, get_baud(baudrate));

        if (tcsetattr(port_handle, TCSANOW, &tty) != 0) {
            error = errno;
            //mexPrintf(strerror(error));
            return error;
        }

        sleep(1);

		is_closed = false;
		return 0;
	}

int get_baud(int baud)
    {
    switch (baud) {
    case 9600:
        return B9600;
    case 19200:
        return B19200;
    case 38400:
        return B38400;
    case 57600:
        return B57600;
    case 115200:
        return B115200;
    case 230400:
        return B230400;
    case 460800:
        return B460800;
    case 500000:
        return B500000;
    case 576000:
        return B576000;
    case 921600:
        return B921600;
    case 1000000:
        return B1000000;
    case 1152000:
        return B1152000;
    case 1500000:
        return B1500000;
    case 2000000:
        return B2000000;
    case 2500000:
        return B2500000;
    case 3000000:
        return B3000000;
    case 3500000:
        return B3500000;
    case 4000000:
        return B4000000;
    default: 
        return -1;
    }
}
};
